package com.example.afomin.understanding_moxy.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.afomin.understanding_moxy.presenters.FourSquaresPresenter;
import com.example.afomin.understanding_moxy.enums.Direction;
import com.example.afomin.understanding_moxy.views.FourSquaresView;
import com.example.afomin.understanding_moxy.R;
import com.example.afomin.understanding_moxy.enums.State;

import androidx.MvpAppCompatActivity;
import androidx.annotation.NonNull;

public class MainActivity extends MvpAppCompatActivity implements FourSquaresView, View.OnClickListener {

    private static final String TAG = "MainActivityDebug";
    @InjectPresenter
    FourSquaresPresenter fourSquaresPresenter;

    Button buttonLeft;
    Button buttonRight;
    Button buttonForward;
    Button buttonBackward;
    Button buttonChangeState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonLeft = findViewById(R.id.buttonLeft);
        buttonRight = findViewById(R.id.buttonRight);
        buttonForward = findViewById(R.id.buttonForward);
        buttonBackward = findViewById(R.id.buttonBackward);
        buttonChangeState = findViewById(R.id.buttonChangeState);

        buttonLeft.setOnClickListener(this);
        buttonRight.setOnClickListener(this);
        buttonForward.setOnClickListener(this);
        buttonBackward.setOnClickListener(this);
        buttonChangeState.setOnClickListener(this);

    }

    @Override
    public void showState(@NonNull State state) {
        switch (state) {
            case CONTROLLABLE: {
                enableComponents();
                break;
            } case DISABLED: {
                disableComponents();
                break;
            } default: {
                throw new IllegalStateException("Illegal State value");
            }
        }
    }

    @Override
    public void showDirection(@NonNull Direction direction) {
        // switch case change color of
        buttonLeft.setBackgroundColor(getResources().getColor(R.color.colorButtonDisabled));
        buttonRight.setBackgroundColor(getResources().getColor(R.color.colorButtonDisabled));
        buttonBackward.setBackgroundColor(getResources().getColor(R.color.colorButtonDisabled));
        buttonForward.setBackgroundColor(getResources().getColor(R.color.colorButtonDisabled));
        switch (direction) {
            case LEFT: {
                buttonLeft.setBackgroundColor(getResources().getColor(
                        R.color.colorButtonEnabled));
                break;
            } case RIGHT: {
                buttonRight.setBackgroundColor(getResources().getColor(
                        R.color.colorButtonEnabled));
                break;
            } case FORWARD: {
                buttonForward.setBackgroundColor(getResources().getColor(
                        R.color.colorButtonEnabled));
                break;
            } case BACKWARD: {
                buttonBackward.setBackgroundColor(getResources().getColor(
                        R.color.colorButtonEnabled));
                break;
            } default: {
                throw new IllegalStateException("Illegal Direction value");
            }
        }
    }

    private void disableComponents() {
        buttonLeft.setEnabled(false);
        buttonRight.setEnabled(false);
        buttonBackward.setEnabled(false);
        buttonForward.setEnabled(false);
    }

    private void enableComponents() {
        buttonLeft.setEnabled(true);
        buttonRight.setEnabled(true);
        buttonBackward.setEnabled(true);
        buttonForward.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        if (fourSquaresPresenter == null) {
            Log.d(TAG, "fourSquaresPresenter is null");
        }
        switch (v.getId()) {
            case(R.id.buttonLeft): {
                fourSquaresPresenter.setDirectionLeft();
                break;
            } case(R.id.buttonRight): {
                fourSquaresPresenter.setDirectionRight();
                break;
            } case(R.id.buttonBackward): {
                fourSquaresPresenter.setDirectionBackward();
                break;
            } case(R.id.buttonForward): {
                fourSquaresPresenter.setDirectionForward();
                break;
            } case(R.id.buttonChangeState): {
                fourSquaresPresenter.setState();
            } default:{ }

        }
    }
}
