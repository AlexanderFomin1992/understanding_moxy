package com.example.afomin.understanding_moxy.views;


import com.arellomobile.mvp.MvpView;
import com.example.afomin.understanding_moxy.enums.Direction;
import com.example.afomin.understanding_moxy.enums.State;

import androidx.annotation.NonNull;

public interface FourSquaresView extends MvpView {
    void showState(@NonNull State state);
    void showDirection(@NonNull Direction direction);
}
