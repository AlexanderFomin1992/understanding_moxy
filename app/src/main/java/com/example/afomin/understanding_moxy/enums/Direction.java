package com.example.afomin.understanding_moxy.enums;

public enum Direction {
    LEFT,
    RIGHT,
    FORWARD,
    BACKWARD
}
