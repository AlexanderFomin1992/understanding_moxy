package com.example.afomin.understanding_moxy.enums;

public enum State {
    DISABLED,
    CONTROLLABLE
}
