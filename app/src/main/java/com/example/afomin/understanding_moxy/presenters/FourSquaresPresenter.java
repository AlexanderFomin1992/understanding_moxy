package com.example.afomin.understanding_moxy.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.afomin.understanding_moxy.views.FourSquaresView;
import com.example.afomin.understanding_moxy.enums.Direction;
import com.example.afomin.understanding_moxy.enums.State;

@StateStrategyType(AddToEndSingleStrategy.class)
@InjectViewState
public class FourSquaresPresenter extends MvpPresenter<FourSquaresView> {
    State state = State.CONTROLLABLE;
    Direction direction = Direction.FORWARD;

//    @ProvidePresenter
//    public FourSquaresPresenter provideFourSquaresPresenter() {
//        return new FourSquaresPresenter();
//    }

    public void setDirectionRight() {
        direction = Direction.RIGHT;
        getViewState().showDirection(direction);
    }
    public void setDirectionLeft() {
        direction = Direction.LEFT;
        getViewState().showDirection(direction);
    }
    public void setDirectionBackward() {
        direction = Direction.BACKWARD;
        getViewState().showDirection(direction);
    }
    public void setDirectionForward() {
        direction = Direction.FORWARD;
        getViewState().showDirection(direction);
    }
    public void setState() {
        switch (state) {
            case DISABLED: {
                state = State.CONTROLLABLE;
                break;
            } case CONTROLLABLE: {
                state = State.DISABLED;
                break;
            } default: {
                throw new IllegalArgumentException("Error! Illegal state value");
            }
        }
    }
}
